# RadUI starter template

快速开发模板：

* 使用 [RadUI](https://gitee.com/harmony-rad/radui) 提供的基础能力，作为commons层的功能。这里演示了 RadTabs, RadList 等组件功能。
* 实现了三层分层架构的模板工程，features中是各个业务模块的独立功能，products层是主包的内容。

## 下载安装

```sh
git clone https://gitee.com/harmony-rad/radui-demos.git
```

## 使用方法

1. agc创建项目，修改 AppScope 中的 appid 信息。修改 modules.json5 中的 client_id 等信息。
2. 修改 build-profile.json5 中的签名信息 signingConfigs 为自己的签名。
3. 本地调试，配置 Deploy Multi Hap 方式调试，实现分包的跳转。
