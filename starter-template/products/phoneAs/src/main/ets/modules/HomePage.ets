import { IconText, RadTabItem, RadTabs } from '@rad/ui';
import ItemsPage from './items/ItemsPage';
import MinePage from './mine/MinePage';

@Builder
function buildHome(item: RadTabItem, index: number, curIndex: number) {
  ItemsPage({ isVisible: index === curIndex });
}

@Builder
function buildMine(item: RadTabItem, index: number, curIndex: number) {
  MinePage({ isVisible: index === curIndex });
}

const TABS_DATA: RadTabItem[] = [
  {
    tabTitle: { icon: $r('app.media.app_icon'), text: $r('app.string.app_name') },
    tabContent: wrapBuilder(buildHome),
  },
  {
    tabTitle: { icon: $r('sys.media.ohos_user_auth_icon_face'), text: $r('app.string.page_mine') },
    tabContent: wrapBuilder(buildMine),
  },
];

@Component
export default struct HomePage {
  @State tabs: RadTabItem[] = TABS_DATA;
  onTabChange = (index: number) => {};

  build() {
    Scroll() {
      Column() {
        RadTabs({
          barPosition: BarPosition.End,
          isBottomTab: true,
          tabs: this.tabs,
          onTabChange: this.onTabChange,
        });
      }
    }.height('100%');
  }
};